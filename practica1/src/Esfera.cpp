// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
	r=g=255;b=0;
	mov=0.025;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(r,g,b);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve()
{
	centro.x+=(velocidad.x*mov);
	centro.y+=(velocidad.y*mov);
}
